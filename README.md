PostgreSQL in Docker Container
=================

Chef Recipe to deploy PostgeSQL database in docker container.

Requirements
------------
**Chef 12.4.x+** is required. 
```
depends 'compat_resource'
depends 'docker', '~> 2.0'
```
# Recipes

### default
The default recipe installs docker in the host and containerizes PostgreSQL database with specified configuration


# Attributes

* `default['postgresql']['superuser'] = 'postgres'`   , Super User name
* `default['postgresql']['password'] = 'superpasswordchangeplease'` , Super User Password
* `efault['postgresql']['db']['default'] = 'postgres'`, Default database
* `default['postgresql']['db']['app'] = 'appdb'`, Application Database
* `default['postgresql']['user'] = 'userapp'`, Database User
* `default['postgresql']['role']['userapp']['password'] = 'anotherpasswordchangeplease'`, UserApp password

* `default['postgresql']['volumes'] = ['/etc/postgresql', '/var/log/postgresql', '/var/lib/postgresql']` ,  list of volumes to mount


* `default['docker-port-map']['db'] = '5432'`, Host port to bind the container's port to. 


# Configuring PostGreSQL

* `templates/make-db-user.sh.erb` contains basic commands to configure the PostgreSQL Database. All commands added to this file will be applied to the database during deployment.

* `files/db_schema.sql` is where you may add your database schema

* `docker_exec 'change host-based authentication rule'` is the resource located in the default recipe that may be used to add custom authentication


## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is free to be used for any learning or educational purpose but cannot be used for commercial use without proper consent of the author.

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required for the project. 