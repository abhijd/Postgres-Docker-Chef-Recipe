# --------------------------------------------------------------------
#             PostgreSQL Parameterss
# --------------------------------------------------------------------
default['postgresql']['superuser'] = 'postgres'               # Super User name
default['postgresql']['password'] = 'somesuperhardpassword'   # Super User Password
default['postgresql']['db']['default'] = 'postgres' # Default database
default['postgresql']['db']['app'] = 'appdb' # Application Database
default['postgresql']['user'] = 'userapp' # Database User
default['postgresql']['role']['userapp']['password'] = 'anotherhardpassword' # UserApp password

default['postgresql']['volumes'] = ['/etc/postgresql', '/var/log/postgresql', '/var/lib/postgresql']


default['docker-port-map']['db'] = '5432'  # Host port to bind the container's port to



