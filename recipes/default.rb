# __________________________________________________________________________________________
#                                                                                           |
# Cookbook Name:: Postgres Docker                                                           |
# Recipe:: postgres_docker                                                                  |
#                                                                                           |
# Copyright Abhijit Das (JD), abhijit.das.jd@gmail.com                                      |
#                                                                                           |
# Licensed under the Apache License, Version 2.0 (the "License");                           |
# you may not use this file except in compliance with the License.                          |
# You may obtain a copy of the License at:                                                  |
#     http://www.apache.org/licenses/LICENSE-2.0                                            |
#                                                                                           |
# Unless required by applicable law or agreed to in writing, software                       |
# distributed under the License is distributed on an "AS IS" BASIS,                         |
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.                  |
# See the License for the specific language governing permissions and                       |
# limitations under the License.                                                            |
#                                                                                           |
#     ----------------------------------------                                              |
#                                                                                           |
# This product includes software developed by Abhijit Das (JD), abhijit.das.jd@gmail.com .  |
# __________________________________________________________________________________________|
#

# --------------------------------------
#   Create needed directory structure
# --------------------------------------
directory '/opt/postgres' do
  recursive   true
  action      :create
end

# ---------------------------------
#     Install and start Docker
# ---------------------------------
docker_service 'default' do
  install_method 'auto'
  service_manager  'auto'
  action           [:create, :start]
end

# ----------------------------------------
#   Pull Docker Image 'postgres:alpine'
# ----------------------------------------
docker_image 'db_image' do
  repo    'postgres'
  tag     'alpine'
  action  :pull_if_missing
end

# ----------------------------------------
#         Create the container
# ----------------------------------------
docker_container 'postgres_container' do
  user        'postgres'
  repo        'postgres'
  tag         'alpine'
  env         ["POSTGRES_DB=#{node['postgresql']['db']['default']}", "POSTGRES_USER=#{node['postgresql']['superuser']} ", "POSTGRES_PASSWORD=#{node['postgresql']['password']}"]
  port        [ "#{node['docker-port-map']['db']}:5432/tcp" ]
  action :create
end

# --------------------------------------------------------------------
#             Adding database altering template file
# --------------------------------------------------------------------
template '/opt/postgres/make-db-user.sh' do
  source    'make-db-user.sh.erb'
  owner     'root'
  group     'root'
  mode      '0755'
  variables(db_name: node['postgresql']['db']['app'],
            user_name: node['postgresql']['user'],
            user_password: node['postgresql']['role']['userapp']['password'])
end

# --------------------------------------------------------------------
#             Adding Database Schema File
# --------------------------------------------------------------------

cookbook_file '/opt/postgres/db_schema.sql' do
  source 'db_schema.sql'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end


execute 'add make-db-user.sh inside container' do
  user 'root'
  command <<-EOF
          docker cp /opt/postgres/make-db-user.sh postgres_container:docker-entrypoint-initdb.d/make-db-user.sh
          docker cp /opt/postgres/db_schema.sql postgres_container:db_schema.sql
      EOF
end


# ---------------------------------
#    Docker Container Started
# ---------------------------------
docker_container 'postgres_container' do
  repo        'postgres'
  tag         'alpine'
  user        'postgres'
  detach      true
  volumes     node['postgresql']['volumes']
  restart_policy 'always'
  action      :start
end

# ------------------------------------------------------------------------
#    Change host based authentication config file to only allow with password
# ------------------------------------------------------------------------

docker_exec 'change host-based authentication rule' do
  container   'postgres_container'
  command     ['sed', "-i 's/trust/md5/g' /var/lib/postgresql/data/pg_hba.conf"]
end

# --------------------------------------------
#    Reload postgres server with new config
# --------------------------------------------
docker_exec 'reload postgres server with new config' do
  container 'postgres_container'
  command   ['pg_ctl', '-D /var/lib/postgresql/data -l logfile reload']
end