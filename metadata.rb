name             'Postgres Docker'
maintainer       'Abhijit Das (JD)'
maintainer_email 'abhijit.das.jd@gmail.com'
source_url       'https://gitlab.com/abhijd/Postgres-Docker-Chef-Recipes'
issues_url       'https://gitlab.com/abhijd/Postgres-Docker-Chef-Recipes'
license          'Apache 2.0'
description      'Chef Cookbook recipe to deploy postgres database in docker container'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.6.03'

depends 'compat_resource'
depends 'docker', '~> 2.0'
